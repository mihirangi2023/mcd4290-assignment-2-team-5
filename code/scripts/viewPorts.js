let list = document.getElementById("port-list");

function assembleListItem(port){
    let elem = document.createElement('li');
    elem.className = "mdl-list__item mdl-list__item--three-line";
    elem.innerHTML =`
        <span class="mdl-list__item-primary-content">
                      <i class="material-icons mdl-list__item-avatar">directions_boat</i>
                      <span>${port.name}</span>
                      <span class="mdl-list__item-text-body">
                        Country:${port.country} <br/>
                        Type:${port.type} | Size: ${port.size}
                      </span>
                    </span>
                    <span class="mdl-list__item-secondary-content">
                      
                    </span>`;
    return elem;
}

function showAllItems(){
    console.log(portList.list);
    for(let i in portList.list){
        list.appendChild(assembleListItem(portList.list[i]));
    }
}

let portList = new PortList();
portList.loadUserPorts();
portList.fetchAPIPorts(showAllItems);