function fetchObject(url, callback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      callback(JSON.parse(this.responseText));
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}

function fetchCoords(location, callback){
    fetchObject(`https://api.opencagedata.com/geocode/v1/json?q=${encodeURI(location)}&key=bb51b165be6e40838b6ad96c16e3d114`, (val)=>{
        let res = val.results;
        if(res.length >= 1){
            callback(res[0].geometry);
        }else{
            callback(null);
        }
    });
}

class Ship{
    constructor(name, maxSpeed, range, description, cost, status){
        this.name = name;
        this.maxSpeed = maxSpeed;
        this.range = range;
        this.description = description;
        this.cost = cost;
        this.status = status;
    }
    
    get name(){
        return this._name;
    }
    
    set name(name){
        if(typeof(name) == 'string'){
            this._name = name;
        }
    }
    
    get maxSpeed(){
        return this._maxSpeed;
    }
    
    set maxSpeed(maxSpeed){
        if(typeof(maxSpeed) == 'number' && maxSpeed > 0){
            this._maxSpeed = maxSpeed;
        }
    }
    
    get maxSpeedKPH(){
        return this.maxSpeed * 1.852;
    }
    
    get range(){
        return this._range;
    }
    
    set range(range){
        if(typeof(range) == 'number' && range > 0){
            this._range = range;
        }
    }
    
    get description(){
        return this._description;
    }
    
    set description(description){
        if(typeof(description) == 'string'){
            this._description = description;
        }
    }
    
    get cost(){
        return this._cost;
    }
    
    set cost(cost){
        if(typeof(cost) == 'number' && cost > 0){
            this._cost = cost;
        }
    }
    
    get status(){
        return this._status;
    }
    
    set status(status){
        if(typeof(status) == 'string'){
            this._status = status;
        }
    }
    
    get PDO(){
        let PDO = {
            name: this.name,
			maxSpeed: this.maxSpeed,
			range: this.range,
			desc: this.description,
			cost: this.cost,
			status: this.status
		};
        return PDO;
    }
    
    set PDO(PDO){
        this.name = PDO.name;
        this.maxSpeed = PDO.maxSpeed;
        this.range = PDO.range;
        this.description = PDO.desc;
        this.cost = PDO.cost;
        this.status = PDO.status;
    }
    
}

class Port{
    constructor(name, country, type, size, lat, long){
        this.name = name;
        this.country = country;
        this.type = type;
        this.size = size;
        this.lat = lat;
        this.long = long;
    }
    
    get name(){
        return this._name;
    }
    
    set name(name){
        if(typeof(name) == 'string'){
            this._name = name;
        }
    }
    
    get country(){
        return this._country;
    }
    
    set country(country){
        if(typeof(country) == 'string'){
            this._country = country;
        }
    }
    
    get type(){
        return this._type;
    }
    
    set type(type){
        if(typeof(type) == 'string'){
            this._type = type;
        }
    }
    
    get size(){
        return this._size;
    }
    
    set size(size){
        if(typeof(size) == 'string'){
            this._size = size;
        }
    }
    
    get lat(){
        return this._lat;
    }
    
    set lat(lat){
        if(typeof(lat) == 'number'){
            this._lat = lat;
        }
    }
    
    get long(){
        return this._long;
    }
    
    set long(long){
        if(typeof(long) == 'number'){
            this._long = long;
        }
    }
    
    get PDO(){
        let PDO = {
          name: this.name,
          country: this.country,
          type: this.type,
          size: this.size,
          lat: this.lat,
          lng: this.long
        };
        return PDO;
    }
    
    set PDO(PDO){
        this.name = PDO.name;
        this.country = PDO.country;
        this.type = PDO.type;
        this.size = PDO.size;
        this.lat = PDO.lat;
        this.long = PDO.lng;
    }
    
}

class Route{
    
    constructor(name, ship, source, dest, startDate, waypoints, distance){
        this.name = name;
        this.ship = ship;
        this.sourcePort = source;
        this.destinationPort = dest;
        this.startDate = startDate;
        this.waypoints = waypoints;
        this.distance = distance;
    }
    
    set name(name){
        if(typeof(name) == 'string'){
            this._name = name;
        }
    }
    
    get name(){
        return this._name;
    }
    
    get sourcePort(){
        return this._sourcePort;
    }
    
    set sourcePort(source){
        if(typeof(source) == 'object'){
            if(source instanceof Port){
                this._sourcePort = source;
            }else{
                let sourcePort = new Port();
                sourcePort.PDO = source;
                this._sourcePort = sourcePort;
            }
        }
    }
    
    get ship(){
        return this._ship;
    }
    
    set ship(ship){
        if(typeof(ship) == 'object'){
            if(ship instanceof Ship){
                this._ship = ship;
            }else{
                let shipTemp = new Ship();
                shipTemp.PDO = ship;
                this._ship = shipTemp;
            }
        }
    }
    
    get destinationPort(){
        return this._destinationPort;
    }
    
    set destinationPort(destination){
        if(typeof(destination) == 'object'){
            if(destination instanceof Port){
                this._destinationPort = destination;
            }else{
                let destinationPort = new Port();
                destinationPort.PDO = destination;
                this._destinationPort = destinationPort;
            }
        }
    }
    
    set startDate(date){
        if(date instanceof Date){
            this._startDate = date;
        }else if(typeof(date) == 'number'){
            this._startDate = new Date(date);
        }
    }
    
    get startDate(){
        return this._startDate;
    }
    
    get distance(){
        return this._distance;
    }
    
    set distance(distance){
        this._distance = distance;
    }
    
    get time(){
        this.distance / this._ship.maxSpeedKPH;
    }
    
    get cost(){
        return this._ship.cost * this.distance;
    }
    
    get waypoints(){
        return this._waypoints;
    }
    
    set waypoints(waypoints){
        this._waypoints = waypoints;
    }
    
    get PDO(){
        let PDO = {
            name: this.name,
            ship: this.ship.PDO,
            source: this.sourcePort.PDO,
            destination: this.destinationPort.PDO,
            startDate: this.startDate.getTime(),
            waypoints: this.waypoints,
            distance: this.distance
        }
        return PDO;
    }
    
    set PDO(PDO){
        this.name = PDO.name;
        this.ship = PDO.ship;
        this.sourcePort = PDO.source;
        this.destinationPort = PDO.destination;
        this.startDate = PDO.startDate;
        this.waypoints = PDO.waypoints;
        this.distance = PDO.distance;
    }
    
}

class ShipList{
    
    constructor(){
        this._list = [];
        this._APICount = 0;
    }
    
    get length(){
        return this._list.length;
    }
    
    get list(){
        return this._list;
    }
    
    removeShip(shipID){
        delete this._list[shipID];
    }
    
    getShip(shipID){
        return this._list[shipID];
    }
    
    addUserShip(ship){
        if(typeof(ship) == 'object'){
            if(!(ship instanceof Ship)){
                let shipTemp = new Ship();
                shipTemp.PDO = ship;
                ship = shipTemp;
            }
            this._list.push(ship);
            return this._list.length - 1;
        }
    }
    
    addAPIShip(ship){
        if(typeof(ship) == 'object'){
            if(!(ship instanceof Ship)){
                let shipTemp = new Ship();
                shipTemp.PDO = ship;
                ship = shipTemp;
            }
            this._list.splice(this._APICount, 0, ship);
            this._APICount++;
            return this._APICount - 1;
        }
    }
    
    loadUserShips(){
        let userShipPDOs = localStorage.getItem("userShips");
        if(userShipPDOs != null){
            userShipPDOs = JSON.parse(userShipPDOs);
            for(let i in userShipPDOs){
                this.addUserShip(userShipPDOs[i]);
            }
        }
    }
    
    saveUserShips(){
        let userShips = this._list.slice(this._APICount);
        let userShipPDOs = [];
        for(let i in userShips){
            if(userShips[i]){
                userShipPDOs.push(userShips[i].PDO);
            }
        }
        localStorage.setItem("userShips", JSON.stringify(userShipPDOs));
    }
    
    loadAPIShips(ships, callback){
        for(let i in ships){
            this.addAPIShip(ships[i]);
        }
        if(callback){
            callback();
        }
    }
    
    async fetchAPIShips(callback){
        fetchObject("https://eng1003.monash/api/v1/ships/", (data)=>{
            this.loadAPIShips(data.ships, callback);
        });
    }
    
}

class PortList{
    
    constructor(){
        this._list = [];
        this._APICount = 0;
    }
    
    get length(){
        return this._list.length;
    }
    
    get list(){
        return this._list;
    }
    
    removePort(portID){
        delete this._list[portID];
    }
    
    getPort(portID){
        return this._list[portID];
    }
    
    addUserPort(port){
        if(typeof(port) == 'object'){
            if(!(port instanceof Port)){
                let portTemp = new Port();
                portTemp.PDO = port;
                port = portTemp;
            }
            this._list.push(port);
            return this._list.length - 1;
        }
    }
    
    addAPIPort(port){
        if(typeof(port) == 'object'){
            if(!(port instanceof Port)){
                let portTemp = new Port();
                portTemp.PDO = port;
                port = portTemp;
            }
            this._list.splice(this._APICount, 0, port);
            this._APICount++;
            return this._APICount - 1;
        }
    }
    
    loadUserPorts(){
        let userPortPDOs = localStorage.getItem("userPorts");
        if(userPortPDOs != null){
            userPortPDOs = JSON.parse(userPortPDOs);
            for(let i in userPortPDOs){
                this.addUserPort(userPortPDOs[i]);
            }
        }
    }
    
    saveUserPorts(){
        let userPorts = this._list.slice(this._APICount);
        let userPortPDOs = [];
        for(let i in userPorts){
            if(userPorts[i]){
                userPortPDOs.push(userPorts[i].PDO);
            }
        }
        localStorage.setItem("userPorts", JSON.stringify(userPortPDOs));
    }
    
    loadAPIPorts(ports, callback){
        for(let i in ports){
            this.addAPIPort(ports[i]);
        }
        if(callback){
            callback();
        }
    }
    
    async fetchAPIPorts(callback){
        fetchObject("https://eng1003.monash/api/v1/ports/", (data)=>{
            this.loadAPIPorts(data.ports, callback);
        });
    }
    
}

class RouteList{
    
    constructor(){
        this._list = [];
    }
    
    get length(){
        return this._list.length;
    }
    
    get list(){
        return this._list;
    }
    
    addRoute(route){
        if(typeof(route) == 'object'){
            if(!(route instanceof Route)){
                let routeTemp = new Route();
                routeTemp.PDO = route;
                route = routeTemp;
            }
            this._list.push(route);
            return this._list.length - 1;
        }
    }
    
    removeRoute(routeID){
        delete this._list[routeID];
    }
    
    getRoute(routeID){
        return this._list[routeID];
    }
    
    loadUserRoutes(){
        let userRoutePDOs = localStorage.getItem("userRoutes");
        if(userRoutePDOs != null){
            userRoutePDOs = JSON.parse(userRoutePDOs);
            for(let i in userRoutePDOs){
                this.addRoute(userRoutePDOs[i]);
            }
        }
    }
    
    saveUserRoutes(){
        let userRoutes = this._list;
        let userRoutePDOs = [];
        for(let i in userRoutes){
            if(userRoutes[i]){
                userRoutePDOs.push(userRoutes[i].PDO);
            }
        }
        localStorage.setItem("userRoutes", JSON.stringify(userRoutePDOs));
    }
}

class Map{
    constructor(mapId, addWaypointCallback, onload){
        mapboxgl.accessToken = 'pk.eyJ1IjoidGVhbTVtb2JpbGVhcHBzIiwiYSI6ImNrY3g5bXBxNzA2Z24yenA3ZnEwdGtxbHkifQ._qQ31ytFs5XliyOcTcjkhA';
        this._map = new mapboxgl.Map({
            container: mapId,
            style: 'mapbox://styles/mapbox/streets-v11'
        });
        if(onload != undefined){
            this._map.on('load', onload);
        }
        //this._mapElem = document.getElementById(mapId);
        this._makers = [];
        this._waypoints = [];
        this._lastLnglat = undefined;
        this._source = undefined;
        this._sourcePoint = undefined;
        this._destination = undefined;
        this._destinationPoint = undefined;
        this._addWaypointCallback = addWaypointCallback;
        
    }
    
    makeWaypointMarker(lng, lat){
        
        let lnglat = mapboxgl.LngLat.convert([lng, lat]);
        
        if(this._lastLnglat != undefined && this._lastLnglat.distanceTo(lnglat) < 50){
            return;
        }
        
        this._waypoints.push([lng, lat]);
        
        // create a HTML element for each feature
        var el = document.createElement('div');
        el.className = 'marker';
        
        // make a marker for each feature and add to the map
        let marker = new mapboxgl.Marker(el);
        marker
            .setLngLat(lnglat)
            .addTo(this._map);
        this._makers.push(marker);
        this.redrawLine();
        if(this._addWaypointCallback != undefined){
            this._addWaypointCallback(this.distance);
        }
    }
    
    makeWaypointMarkers(lnglats){
        for(let i in lnglats){
            this.makeWaypointMarker(...lnglats[i]);
        }
    }
    
    addClickListener(){
        this._map.on('click', (e)=>{
            this.makeWaypointMarker(e.lngLat.lng, e.lngLat.lat);
        });
    }
    
    clearAllMarkers(){
        for(let i in this._makers){
            this._makers[i].remove();
        }
        this._makers = [];
        this._waypoints = [];
        this.redrawLine();
    }
    
    flyTo(lng, lat){
        this._map.flyTo({
            center: [lng, lat]
        });
    }
    
    makePortMarker(lng, lat){
        let lnglat = mapboxgl.LngLat.convert([lng, lat]);
        // create a HTML element for each feature
        var el = document.createElement('div');
        el.className = 'port-marker';

        // make a marker for each feature and add to the map
        let marker = new mapboxgl.Marker(el);
        marker
            .setLngLat(lnglat)
            .addTo(this._map);
        return marker;
    }
    
    setSource(lng, lat){
        if(!this._source){
            this._source = this.makePortMarker(lng, lat);
        }else{
            let lnglat = mapboxgl.LngLat.convert([lng, lat]);
            this._source.setLngLat(lnglat);
        }
        this._sourcePoint = [lng, lat];
        this.flyTo(lng, lat);
        if(this._addWaypointCallback != undefined){
            this._addWaypointCallback(this.distance);
        }
        this.redrawLine();
    }
    
    setDestination(lng, lat){
        if(!this._destination){
            this._destination = this.makePortMarker(lng, lat);
        }else{
            let lnglat = mapboxgl.LngLat.convert([lng, lat])
            this._destination.setLngLat(lnglat);
        }
        this._destinationPoint = [lng, lat];
        if(this._addWaypointCallback != undefined){
            this._addWaypointCallback(this.distance);
        }
        this.redrawLine();
    }
    
    get waypoints(){
        return this._waypoints;
    }
    
    get distance(){
        let points = [this._sourcePoint, ...this._waypoints, this._destinationPoint];
        let distance = 0;
        for(let i = 0; i < points.length; i++){
            if(points[i] != undefined && points[i+1] != undefined){
                let point1 = mapboxgl.LngLat.convert(points[i]);
                let point2 = mapboxgl.LngLat.convert(points[i+1]);
                distance += point1.distanceTo(point2);
            }
        }
        return distance/1000;
    }
    
    redrawLine(){
        let points = [this._sourcePoint, ...this._waypoints, this._destinationPoint];
        let coords = [];
        for(let i in points){
            if(points[i] != undefined){
                coords.push(points[i]);
            }
        }
        if(this._map.getLayer('route')){
            this._map.removeLayer('route');
        }
        if(this._map.getSource('route')){
            this._map.removeSource('route');
        }
        this._map.addSource('route', {
                'type': 'geojson',
                'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                'type': 'LineString',
                'coordinates': coords
                }
                }
            });
        this._map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route',
                'layout': {
                'line-join': 'round',
                'line-cap': 'round'
                },
                'paint': {
                'line-color': '#888',
                'line-width': 8
                }
            });
    }
    
}

//let shipList = new ShipList();
//shipList.fetchAPIShips();
//shipList.loadUserShips();
//
//let portList = new PortList();
//portList.fetchAPIPorts();
//portList.loadUserPorts();
//
//let routeList = new RouteList();
//routeList.loadUserRoutes();
