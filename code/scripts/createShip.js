let nameElem = document.getElementById("ship-name");
let maxSpeedElem = document.getElementById("max-speed");
let rangeElem = document.getElementById("ship-range");
let costElem = document.getElementById("ship-cost");
let descElem = document.getElementById("ship-description");

document.getElementById("create-ship-button").onclick = ()=>{
    let name = nameElem.value;
    let maxSpeed = Number(maxSpeedElem.value);
    if(isNaN(maxSpeed)){
        return alert("Invalid Maximum Speed!");
    }
    let cost = Number(costElem.value);
    if(isNaN(cost)){
        return alert("Invalid Cost!");
    }
    let range = Number(rangeElem.value);
    if(isNaN(range)){
        return alert("Invalid range!");
    }
    let desc = descElem.value;
    let ship = new Ship(name, maxSpeed, range, desc, cost, "available");
    shipList.addUserShip(ship);
    shipList.saveUserShips();
    location.href = "viewShips.html";
    
};

let shipList = new ShipList();
shipList.loadUserShips();