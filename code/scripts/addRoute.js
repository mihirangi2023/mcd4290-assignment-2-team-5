function setDistance(distance){
    document.getElementById("distance").innerHTML=distance.toFixed(3) +" km";
    if(ship == undefined || ship.range < distance){
        alert("Please select another ship!");
    }
    hideShipsBelowDistance(distance);
    route.distance = distance;
    calculateCost();
}

function calculateCost(){
    if(route.ship != undefined){
        document.getElementById("cost").innerHTML = "$"+route.cost.toFixed(2);
    }
}

let map = new Map("map", setDistance);
map.addClickListener();

let sourceElem = document.getElementById("source");
let destElem = document.getElementById("destination");
let shipElem = document.getElementById("ship");
let dateElem = document.getElementById("date");
let loaded = 0;
let shipOptions = [];

document.getElementById("create-route-button").onclick = ()=>{
    if(loaded < 2){
        return alert("Please wait for ships and ports to load!");
    }
    let date = dateElem.value;
    if(date === ''){
        return alert("Please select a start date!");
    }
    date = new Date(date);
    let ship = shipList.list[Number(shipElem.value)];
    let source = portList.list[Number(sourceElem.value)];
    let dest = portList.list[Number(destElem.value)];
    let route = new Route(name, ship, source, dest, date, map.waypoints);
    routeList.addRoute(route);
    routeList.saveUserRoutes();
    location.href = "index.html";
}

function createOption(id, name){
    let elem = document.createElement("option");
    elem.setAttribute("value", String(id));
    elem.innerHTML = name;
    return elem;
}

function hideShipsBelowDistance(distance){
    if(ship == undefined || ship.range < distance){
        ship = undefined;
    }
    for(let i in shipList.list){
        if(shipList.list[i].range < distance){
           shipOptions[i].style.display = "none";
        }else{
            shipOptions[i].style.display = "block";
            if(ship == undefined){
                ship = shipList.list[i];
            }
        }
    }
}

function showShips(){
    ship = shipList.list[0];
    route.ship = ship;
    for(let i in shipList.list){
        let elem = createOption(i, shipList.list[i].name);
        shipElem.appendChild(elem);
        shipOptions.push(elem);
    }
    loaded++;
}

function showPorts(){
    let port = portList.list[0];
    map.setSource(port.long, port.lat);
    map.setDestination(port.long, port.lat);
    route.destinationPort = port;
    route.sourcePort = port;
    map.clearAllMarkers();
    for(let i in portList.list){
        destElem.appendChild(createOption(i, portList.list[i].name));
        sourceElem.appendChild(createOption(i, portList.list[i].name));
    }
    loaded++;
}

sourceElem.onchange = ()=>{
    let port = portList.list[Number(sourceElem.value)];
    map.setSource(port.long, port.lat);
    map.clearAllMarkers();
    route.sourcePort = port;
}

destElem.onchange = ()=>{
    let port = portList.list[Number(destElem.value)];
    map.setDestination(port.long, port.lat);
    map.clearAllMarkers();
    route.destinationPort = port;
}

shipElem.onchange = ()=>{
    ship = shipList.list[Number(shipElem.value)];
    if(ship.range < distance){
        alert("Please select another ship!");
    }
    route.ship = ship;
    calculateCost();
}

let ship, route;
route = new Route();

let shipList = new ShipList();
shipList.loadUserShips();
shipList.fetchAPIShips(showShips);

let portList = new PortList();
portList.loadUserPorts();
portList.fetchAPIPorts(showPorts);



let routeList = new RouteList();
routeList.loadUserRoutes();
