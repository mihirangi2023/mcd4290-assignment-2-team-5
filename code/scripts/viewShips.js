let list = document.getElementById("ship-list");

function assembleListItem(ship){
    let elem = document.createElement('li');
    elem.className = "mdl-list__item mdl-list__item--three-line";
    elem.innerHTML =`
        <span class="mdl-list__item-primary-content">
              <i class="material-icons mdl-list__item-avatar">directions_boat</i>
              <span>${ship.name}</span>
              <span class="mdl-list__item-text-body">
                ${ship.description} <br/>
                Max Speed:${ship.maxSpeed} Knots | Cost: ${ship.cost} per Km | Range: ${ship.range} Km
              </span>
        </span>
        <span class="mdl-list__item-secondary-content">
        </span>`;
    return elem;
}

function showAllItems(){
    console.log(shipList.list);
    for(let i in shipList.list){
        list.appendChild(assembleListItem(shipList.list[i]));
    }
}

let shipList = new ShipList();
shipList.loadUserShips();
shipList.fetchAPIShips(showAllItems);


