function setDistance(distance){
    document.getElementById("distance").innerHTML=distance.toFixed(3) +" km";
    route.distance = distance;
    document.getElementById("cost").innerHTML = "$"+route.cost.toFixed(2);
}

function showRoute(route){
    //document.getElementById("distance")
    document.getElementById("source").innerHTML = `${route.sourcePort.name} - ${route.sourcePort.country}`;
    document.getElementById("destination").innerHTML = `${route.destinationPort.name} - ${route.destinationPort.country}`;
    document.getElementById("ship").innerHTML = route.ship.name;
    let date = route.startDate;
    let dateStr = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
    document.getElementById("date").innerHTML = dateStr;
    map.makeWaypointMarkers(route.waypoints);
    let source = route.sourcePort;
    map.setSource(source.long, source.lat);
    let destination = route.destinationPort;
    map.setDestination(destination.long, destination.lat);
}

let routeNum = localStorage.getItem("selected-route");
if(routeNum == null){
    location.href = "index.html";
}

let routeList = new RouteList();
routeList.loadUserRoutes();
let route = routeList.list[Number(routeNum)];
let map = new Map("map", setDistance, showRoute.bind(null, route));

document.getElementById("delete-button").onclick = ()=>{
    routeList.removeRoute(routeNum);
    routeList.saveUserRoutes();
    localStorage.removeItem("selected-route");
    location.href = "index.html";
}