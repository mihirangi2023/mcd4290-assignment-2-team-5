let list = document.getElementById("route-list");

function assembleListItem(route, id){
    let elem = document.createElement('li');
    elem.className = "mdl-list__item mdl-list__item--three-line";
    let date = route.startDate;
    let dateStr = `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
    elem.innerHTML =`
        <li class="mdl-list__item mdl-list__item--three-line">
                    <span class="mdl-list__item-primary-content">
                      <i class="material-icons mdl-list__item-avatar">directions_boat</i>
                      <span>${route.sourcePort.name}  - ${route.destinationPort.name}</span>
                      <span class="mdl-list__item-text-body">
                        Ship: ${route.ship.name} <br/>
                        Date: ${dateStr}
                      </span>
                    </span>
                    <span class="mdl-list__item-secondary-content">
                      
                    </span>
                  </li>`;
    elem.onclick = ()=>{
        localStorage.setItem("selected-route", String(id));
        location.href = "viewRoute.html";
    }
    return elem;
}

function showAllItems(){
    
    for(let i in routeList.list){
        list.appendChild(assembleListItem(routeList.list[i], i));
    }
}

let routeList = new RouteList();
routeList.loadUserRoutes();
showAllItems();