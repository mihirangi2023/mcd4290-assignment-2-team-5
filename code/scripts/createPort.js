let nameElem = document.getElementById("port-name");
let countryElem = document.getElementById("port-country");
let typeElem = document.getElementById("port-type");
let sizeElem = document.getElementById("port-size");
let locationElem = document.getElementById("port-location");

function createPort(location){
    if(!location){
        alert("Invalid location");
    }else{
        let name = nameElem.value;
        let country = countryElem.value;
        let type = typeElem.value;
        let size = sizeElem.value;
        let port = new Port(name, country, type, size, location.lat, location.lng);
        portList.addUserPort(port);
        portList.saveUserPorts();
        location.href = "viewPorts.html";
    }
}

document.getElementById("create-port-button").onclick = ()=>{
    fetchCoords(locationElem.value, createPort);
}

let portList = new PortList();
portList.loadUserPorts();